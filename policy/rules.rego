package main

# Accessors

resource_main := resource("aws_ram_resource_share", "main")

resource_subnet_association := resource("aws_ram_resource_association", "subnet_association")

resource_accounts := resource("aws_ram_principal_association", "accounts")

resource_orga := resource("aws_ram_principal_association", "orga")

output_share_id := output("share_id")

variable_share_name := variable("share_name")

variable_subnet_id := variable("subnet_id")

variable_allow_externals := variable("allow_externals")

variable_organization_arn := variable("organization_arn")

variable_accounts_arn := variable("accounts_arn")

# Rules

deny[msg] {
  count(resource_main) != 1
  msg = "Define the main resource share"
}

deny[msg] {
  count(resource_subnet_association) != 1
  msg = "Define the subnet resource association"
}

deny[msg] {
  count(resource_accounts) != 1
  msg = "Define the accounts principal association"
}

deny[msg] {
  count(resource_orga) != 1
  msg = "Define the orga principal association"
}

deny[msg] {
  count(variable_share_name) != 1
  msg = "Define the share_name variable"
}

deny[msg] {
  count(variable_subnet_id) != 1
  msg = "Define the subnet_id variable"
}

deny[msg] {
  count(variable_allow_externals) != 1
  msg = "Define the allow_externals variable"
}

deny[msg] {
  count(variable_organization_arn) != 1
  msg = "Define the organization_arn variable"
}

deny[msg] {
  count(variable_accounts_arn) != 1
  msg = "Define the accounts_arn variable"
}

deny[msg] {
  count([value | value := output_share_id[_].value; value == "${aws_ram_resource_share.main.id}"]) != 1
  msg = "The share_id output should use the main resource share for its value"
}

deny[msg] {
  count([value | value := resource_main[_].name; value == "${var.share_name}"]) != 1
  msg = "The main resource share must use the share_name variable as its name"
}

deny[msg] {
  count([value | value := resource_main[_].allow_external_principals; value == "${var.allow_externals}"]) != 1
  msg = "The main resource share must use the allow_externals variable as its allow_external_principals"
}

deny[msg] {
  count([value | value := resource_subnet_association[_].resource_arn; value == "${var.subnet_id}"]) != 1
  msg = "The subnet resource association must use the subnet_id variable as its resource_arn"
}

deny[msg] {
  count([value | value := resource_subnet_association[_].resource_share_arn; value == "${aws_ram_resource_share.main.id}"]) != 1
  msg = "The subnet resource association must use the main resource share id as its resource_share_arn"
}

deny[msg] {
  count([value | value := resource_accounts[_].count; value == "${length(var.accounts_arn)}"]) != 1
  msg = "The accounts principal association must iterate over the accounts_arn variable"
}

deny[msg] {
  count([value | value := resource_accounts[_].principal; value == "${element(var.accounts_arn, count.index)}"]) != 1
  msg = "The accounts principal associtation must use the accounts_arn variable iterator as principal"
}

deny[msg] {
  count([value | value := resource_accounts[_].resource_share_arn; value == "${aws_ram_resource_share.main.id}"]) != 1
  msg = "The accounts principal association must use the main resource share id as its resource_share_arn"
}

deny[msg] {
  count([value | value := resource_orga[_].principal; value == "${var.organization_arn}"]) != 1
  msg = "The orga principal association muse use the organization_arn variable as its principal"
}

deny[msg] {
  count([value | value := resource_orga[_].resource_share_arn; value == "${aws_ram_resource_share.main.id}"]) != 1
  msg = "The orga principal association must use the main resource share id as its resource_share_arn"
}