// Declare resource share

resource "aws_ram_resource_share" "main" {
  name                      = var.share_name
  allow_external_principals = var.allow_externals
}

// Associate subnet with resource share

resource "aws_ram_resource_association" "subnet_association" {
  resource_arn       = var.subnet_id
  resource_share_arn = aws_ram_resource_share.main.id
}

// Share with specified accounts

resource "aws_ram_principal_association" "accounts" {
  count = length(var.accounts_arn)

  principal          = element(var.accounts_arn, count.index)
  resource_share_arn = aws_ram_resource_share.main.id
}

// Share with organization

resource "aws_ram_principal_association" "orga" {
  count = var.organization_arn != "false" ? 1 : 0

  principal          = var.organization_arn
  resource_share_arn = aws_ram_resource_share.main.id
}

