output "share_id" {
  description = "ARN of the resource share."
  value       = aws_ram_resource_share.main.id
}

