variable "share_name" {
  type        = string
  description = "Name of the resource share."
  default     = "main"
}

variable "subnet_id" {
  type        = string
  description = "ID of the shared subnet."
}

variable "allow_externals" {
  description = "Allow sharing with externals account (bool)."
  type        = string
  default     = "false"
}

variable "organization_arn" {
  description = "ID of the organization (Sharing must be enabled at organization level : https://console.aws.amazon.com/ram/home#Settings)."
  default     = "false"
}

variable "accounts_arn" {
  description = "Share the subnet with this list of accounts ARN."
  type        = list(string)
  default     = []
}

